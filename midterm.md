# Markdown and Various Flavors
> Author: Dillon Lindsay

Markdown is a common syntax for quickly creating documents that include code snippets, regular text, and various text elements. R Markdown is a library that allows the conversion and execution of various code syntaxes from a singluar markdown file.

# Headers & Titles
Titles are defined using a `#` symbol.
```
One # is the largest heading:
# Heading 1
Two is slightly smaller, etc.
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
```
One # is the largest heading:
# Heading 1
Two is slightly smaller:
## Heading 2
So on and so on
### Heading 3
#### Heading 4
##### Heading 5

# Interpreted Code Chunks
R Markdown powerfully combines code snippets with actual interpreted code results. The basic syntax for **interpreted** R code is:
```
    ```{r}
    df <- read_table("mydata.csv", headers = TRUE, sep = ",")
    ```
```

These are known as 'chunks' and they have a lot of options. First you can name a chunk by adding any characters after the 'r' delimiter and before the end brace:
```
    ```{r my_chunk}
    df$some_column
    ```
```

Chunk options allow for customized output and are extremely handly for report writing. I find it helpful to separate them with a comma, although not necessary.
```
    ```{r data, echo=FALSE}
    my_data <- read_table("data.csv", header=TRUE, sep=',')
    ```
```

The most common are probably:
1. **echo**: If false, hides source code from output.
2. **include**: If true, chunk is included in final document. If false, chunk will run but not be considered part of the source code.
3. **comment**: prepended characters for each output line. Set to '' if you want no comment.
4. **warning**: If false, will hide any warnings generated by your code.
5. **message**: If false, will hide any messages generated by your code.
6. **ref.label**: My favorite chunk option. It should equal an array of chunk names. Each chunk that you include, will be interpreted in this chunk. So if you use `eval=FALSE` then you can create your own code appendix with one line of code (making sure echo is set to true).

```
     ```{r appendix, ref.label=c('read_data', 'clean_data', 'fit_model', 'features'), eval=FALSE}
     ```
```

## How R Markdown works
R Markdown leverages a library called 'knitr' which takes the raw text of your R Markdown file and hands off different pieces of it to different interpreters. R chunks are given to the R interpreter, LaTeX equations are given to a LaTeX interpreter, Python chunks to a Python interpreter, etc. Raw text is perserved for the final output. This process is known as __kniting__ and is unique to the 'knitr' package and almost all other markdown documents. After each 'chunk' of your R Markdown is interpreted, the results are returned and knitr will handle the returned output according to your instructions. Most of this is handled by default behavior so you don't have to worry about it, but you can change almost everything with some settings if you want.

For example, if you want to set one option for all chunks then you can use the function `knitr::opts_chunk$set(comment = '')` in an R code chunk.

If you want to interpret R code within text (i.e. outside a code chunk) then you can include a single back tick followed by 'r' and the code you want. This code shares the same environment as the rest of your code.

```
The R-Squared of my model is: `r my_model$fit`.
```

R Markdown uses a header delimited with three `---` which is used when your file is 'knited'. If you use RStudio, most of these options are created for you, but you can always change them on your own or add others whenever you'd like. All of these options are only read when you knit your file. Here are some of the most common:
1. **Author**: Will be used as a header for your output.
2. **Output**: The type of document that you want created from your file.
3. **Title**: Will be used as a title element in your output.
4. **params:**: Any variable names given on a new line, indented with a space, and followed by a colon and corresponding value will be saved in your environment. If you need R to interpret any parameter (e.g. if your parameter is the result of a function call) then include `!language_name ` before the function call. To access this parameters inside your file, they will be stored in a data frame called 'params'.
```
---
params:
 n: 100
 d: !r Sys.Date()
---
```
```
Today's date is `r params$d`
```

# Code Snippets
Outside of R Markdown, you can demonstrate code without using the braces to delimit the language name. This will not interpret or run the code, but does support syntax highlighting.
```
    ```python
    print("Hello,", "world!")
    ```
```

```python
print("Hello,", "world!")
```

```
In-line code sections are the same but without a language name: `from pandas import DataFrame`
```
In-line code sections are the same but without a language name: `from pandas import DataFrame`

# Tex Equations
Pandoc allows for both simple and multi-line LaTeX equations made right within your markdown. A simple LaTeX equation is created with a single '$' and then terminated with another. `$E(x)=(X-\mu)/n$`
Complex or multi-line LaTeX equations are created with two '$' and terminated likewise. `$$y=\beta_{1}x + \beta_{0}$$`
This format is also useful if you want your equation indented like a quote. If you don't know LaTex, then don't worry about anything else. If you do understand LaTeX, it's important to note that any LaTeX here is only executed in 'equation' mode.

Herein lies the power of using knitr because the LaTeX characters will be handed off to the LaTeX interpreter and the result is then returned to the rendered version of your file.

# Tables
Tables are not generally a part of markdown syntax but common flavors have support for it. Simple tables can be created by using the vertical bar character '|'. Table columns are separated by this character and the column headers are separated by a line of dashes '-' and vertical bars designating the columns.
```
| Tables        | Are           | Useful |
| ------------- |:-------------:|-------:|
| column 3 is   | right-aligned | 3,400  |
| column 2 is   | centered      | 24     |
| markdown      | is cool       | 1      |
```
| Tables        | Are           | Useful |
| ------------- |:-------------:|-------:|
| column 3 is   | right-aligned | 3,400  |
| column 2 is   | centered      | 24     |
| markdown      | is            | cool   |

In R Markdown, more complex tables or easy tables from R dataframes can be created using various packages. Knitr has its own function which I have found is the simplest. `knitr::kable(data, caption="Table with Kable")`.

# Images
Images can be included by starting the line with `![caption name](path/to/image/or/html)`.
![R Markdown CheatSheet1](./rmarkdown-2.0-page-001.jpg)
![R Markdown CheatSheet2](./rmarkdown-2.0-page-002.jpg)

# Text Formatting
```
To start a numbered list, simply start the line with the number followed by a period.
1. Item 1
2. Item 2
```

To start a numbered list, simply start the line with the number followed by a period.
1. Item 1
2. Item 2

The flavor of markdown used by knitr is called "Pandoc's" and allows you to create a list item that can be interrupted using: `@.`.

@. Item 1
Interupting paragraph.
@. Item 2

Note here that GitHub flavored markdown or Markdown Here does not support these kind of list items.

```
> Can be used to delimit a block quote. Text here will wrap around to a new line.
```

> Can be used to delimit a block quote. Text here will wrap around to a new line.

```
Footnotes can be marked[^1]
```

Footnotes can be marked[^1]

```
[^1]: Here is the footnote.
```

[^1]: Here is the footnote.
